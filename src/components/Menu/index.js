import React, { useEffect, useState } from "react";

import { Link } from "react-router-dom";

import { useHistory } from "react-router-dom";

import ContainerMenu from "./styled";

import Home from "./../../assets/home.png";

import Logout from "./../../assets/logout.png";

import Events from "./../../assets/events.png";

import CIP from "./../../assets/cip-logo.png";

import Api from "./../../config/api";

import { getToken } from "./../../config/auth";

export default function Index() {
    return (
        <>
            <ContainerMenu>
                <div className="menu-grid">
                    <header>
                        <section>
                            <img
                                id="infouser"
                                src={CIP}
                                alt=""
                            ></img>
                        </section>
                        
                    </header>

                    <main>
                        <nav>
                            <ul>
                                <Link
                                    style={{ textDecoration: "none" }}
                                    to={{ pathname: "/dashboard" }}
                                >
                                    <li className="first">
                                        <img src={Home} alt=""></img>
                                    </li>
                                </Link>
                                <Link
                                    style={{ textDecoration: "none" }}
                                    to={{ pathname: "/event/add" }}
                                >
                                    <li>
                                        <img src={Events} alt=""></img>
                                    </li>
                                </Link>
                                <Link
                                    style={{ textDecoration: "none" }}
                                    to={{ pathname: "/" }}
                                >
                                    <li className="last">
                                        <img src={Logout} alt=""></img>
                                    </li>
                                </Link>
                            </ul>
                        </nav>
                    </main>
                </div>
            </ContainerMenu>
        </>
    );
}

import Styled from "styled-components";

const OngoingStyled = Styled.div`


    main{
        padding-bottom: 1.3em;

        div.header-boxes{
            padding:1.8em 0;
        }

        div.header-main-boxes{
            box-sizing: content-box;
            padding: 0 2em;
            height: 100px;
            position: relative;
            h3{
                width: 50%;
                padding: 0 1rem;
                border: 2px solid #364066;
                font-size: 19px;
            }

        }

        div.timeline{
            display:inherit;
            flex-direction: column;
            align-items: center;
            

            div.send-comments{
                position: relative;
                padding: 0em 0em 4em 0em;
                z-index:999;
                display: flex;

                input{
                    position:absolute;
                    width:97%;
                }

                div.input{
                    right: -1px;
                    width: 16%;
                    height: 77%;
                    z-index: 9999;
                    position: absolute;
                    cursor: pointer;
                    display: flex;
                    background: #07075936;
                    justify-content: space-evenly;
                    align-items: center;
                    border: 1px solid #ccc;
                    border-radius: 5px;

                    cursor:pointer;
                    img{
                        padding: 0.4em 0 0 0;
                    }
                }
            }

        }

        div.main-grid-box{
            display:flex;
            justify-content:center;
            
            div.box{
                height: 300px;
                float: left;
                padding: 2em 1em;
                margin: 1.5em 1em 0;
                border-radius: 5px;
                box-sizing: content-box;
                display: flex;
                background:linear-gradient(315deg, #2D4F7A, #5CA1FA);
                flex-direction: column;
                position: relative;
                z-index: 1;

                &:before{
                    content:'';
                    top:-2px;
                    left:-2px;
                    height:100%;
                    width:100%;
                    bottom:-2px;
                    position:absolute;
                    background:#fff;
                    transform:skew(2deg,2deg);
                    z-index: -1;
                }
                


                nav{
                    display: flex;
                    align-items: center;
                    margin-top: 2rem;
                    padding: 0 1em;
                    background: #2D4F7A;
                    color: #fff;
                    border: 1px transparent;
                    border-radius: 6px;
                    ul{
                        list-style:none;
                    }

                    li{
                        margin:1em 0;
                        &:before{
                            content:"";
                            width: 20px;
                            height: 9px;
                            float:left;
                            position:relative;
                            top:7px;
                            display: flex;
                            border: 1px transparent;
                            border-radius: 15px;
                        }

                        b{
                            float:left;
                            margin:0 1em;
                            width: 155px;
                        }
                    }
                }
                
                header.box-header{
                    border: 3px solid;
                    padding: 15px;
                    b{
                        font-size:15px;
                        font-weight: 900;
                    }
                }
            }

            div.box-comments{
                display:flex;
                overflow:none;
            
                section.comments{
                    height:400px;
                    width: 98%;
                    padding:1em 0.5em;
                    overflow:auto;
                    border: 2px solid #ccc;
                    background-color:#fff;

                    div.comments{
                        background-color: rgba(210, 210, 210, 0.41);
                        padding: 0 1em;
                        font-stretch: unset;
                        font-size: 0.9em;
                        text-align: justify;
                        margin: 2em 1em;
                        border: 1px solid #fff7f7;
                        border-radius: 7px;
                        box-shadow: 5px 6px 9px #ccc;

                        div.header-comments{
                            margin: 0;
                            padding: 10px 0;
                            border-bottom: 3px solid #364061;
                            b{
                                color: #5d5c5c;
                                padding: 0.2em;
                            }
                        }

                        div.main-comments{
                            padding: 0.2em;
                        }
                    }
                }

                div.details-text-area{
                    height: 40%;
                    margin: 3em 0 0 0;
                    align-items: center;
                    display :flex;
                    flex-direction: column;
                    justify-content : center;

                    label{
                        font-weight:600;
                    }
                    textarea{
                        padding:0 0 0 1em ;
                        height: 200px;
                        padding: 0.5em 0 0.5em 0.5em;
                        height: 200px;
                        width: 90%;
                        background:transparent;
                        box-shadow: 1px 3px 7px #0b3c7173;;

                        font-family:'Roboto';
                        text-align: justify;
                    }

                    button.button-comments{
                        margin-top:2.5em;
                    }
                }

            }
        }

        div.details{
            height: 80px;
        }


        button.telegram{
            position: fixed;
            bottom: 1em;
            right: 25px;
        }

        div.popup{
            position: absolute;
            width: 100%;
            top: 0;
            left: 0;
            right: 0;
            height: 100%;
            overflow: hidden;
            background: #000000a6;
            z-index: 9999;
        }
    }
`;

export default OngoingStyled;

import Styled from 'styled-components';


const newUserStyled = Styled.div`
    display:flex;
    flex-direction:column;
    background-blend-mode: color-burn;
    background-clip: content-box;

    div.div-grid-geral{
        text-align:center;
        display: flex;
        flex-direction: column;
        padding:0;

        header{
            background:#f1f1f1;
            margin-bottom:3em;
            border-bottom:3px solid #d0deec;
            h2{
                color: #111315cf;
                font-size:24px;
                margin: 0;
            }

        }
        form{
            height: 70%;
            width: 80%;
            margin: 0 auto;

           

            .row{
                border:2px solid #d0deec;
                border-radius:13px;
                margin:1em 0pt;
                padding:2em 1em;
                text-align:justify;
                display:flex;
                flex-direction:column;
                justify-content:center;
                box-shadow:3px 4px 11px #e4e0e0;
                
                p{
                    font-size: 2em;
                }

                div.box-information-login,
                div.box-information-people{
                    width:100%;
                    display:flex;
                    margin-left: 5em;
                    flex-direction:column;
                }
                
                label{
                    margin-top: 0.5em;
                    width: 200px;
                    font-size: 14px;
                    border-bottom: 4px solid #2e2f2bcc;
                    border-radius: 3px;
                    padding-left: 0.3em;
                }
                input,select{
                    background:transparent;
                    margin:1em;
                    width:70%;
                    color:#2e2f2bcc;
                    height:15px;
                    box-shadow:4px 3px 5px #ccc;
                    
                }

                button{
                    width:200px;
                    margin:1em auto;
                }
                
            }
        }






    }

`;


export default newUserStyled;
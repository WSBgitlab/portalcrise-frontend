import React from "react";

const DatasPaginations = ({ dataPerPage, totalPages, paginate }) => {
    const pageIndexes = [];

    for (let i = 1; i <= Math.ceil(totalPages / dataPerPage); ++i) {
        pageIndexes.push(i);
    }

    return (
        <nav>
            <ul className="pagination">
                {pageIndexes.map((index) => (
                    <li key={index} className="page-item">
                        <a
                            onClick={() => paginate(index)}
                            href="!#"
                            className="page-link"
                        >
                            {index}
                        </a>
                    </li>
                ))}
            </ul>
        </nav>
    );
};

export default DatasPaginations;

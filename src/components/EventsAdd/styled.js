import styled from "styled-components";

const StyleAddEvents = styled.div`
    .span-form {
        display: none;
    }
    .show-others-sistem {
        display: flex !important;
        height: 100px;
        top: 24px;
        flex-direction: column;

        padding: 0 0 0 0;
        display: flex !important;
        justify-content: center;

        input.others-sistem {
            margin-top: 2em;
            width: 100%;
        }

        button {
            margin: 1em auto;
        }
    }

    header {
        width: 50%;
        height: 50px;
        margin: 1em auto;

        span {
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100%;
            h4 {
                margin: 0;
            }
        }

        hr {
            height: 4px;
            border: 1px solid #f67e7e;
            border-radius: 10px;
            background-color: #f67e7e;
        }
    }

    main.addmenu {
        padding: 2em 0;
        display: flex;
        justify-content: center;
        background-color: rgba(196, 196, 196, 0.18);
        margin: 3em 0;
        padding: 2em;
        box-shadow: 4px 4px 11px #cccc;
        box-sizing: border-box;

        div.servicos {
            display: flex;
            flex-direction: column;
            background: #ffff;
            border-radius: 4px;
            padding: 1rem;
            border: 4px solid #1d477c73;
            select {
                height: 18px;
                width: 89%;
                box-sizing: content-box;
                box-shadow: 2px 3px 5px #d0d0d0;
            }

            label.label-form-color {
                color: #3c3939c7;
            }

            .div-label-form-events {
                width: 80%;
                margin: 1em auto;
                position: relative;
                span {
                    position: absolute;
                    top: -5px;
                    right: 0;
                    img {
                        width: 20px;
                        cursor: pointer;
                    }
                }

                div.question {
                    position: absolute;
                    top: 25px;
                    width: 526px;
                    color: #fff;
                    background: #34325a;
                    z-index: 99999;
                    right: 0;
                    left: -30px;
                    border: 1px solid #fff;
                    border-radius: 6px;
                    box-sizing: content-box;
                    padding: 1.5em 2.5rem;

                    > strong {
                        margin: 20px 0;
                        font-size: 22px;
                    }
                }
            }
        }

        .display-none {
            display: none;
            transition: all 0.6s;
        }

        .display-flex {
            display: flex;
            flex-direction: column;
            padding: 10px 0;

            input {
                height: 12px;
            }
        }

        .section-popup{
            height:300px;
        }
        
        form.form-add-event {
            width: 80%;
            position:relative;
            display: flex;
            flex-direction: column;
            justify-content: center !important;
            margin: 0 auto;
            background: #ffff;
            border-radius: 4px;
            border: 1px solid #fff0;

            .add-aux-info {
               position:absolute;
               top: 3rem;
               left:8px;

               span{
                    position:absolute;

                    img{
                        top:0;
                        width: 20px;
                    }
               }
            }

            button.btn-rto{
                font-size:12px;
                padding:5px;
            }

            

            

            select {
                height: 22px;
                box-sizing: content-box;
                box-shadow: 2px 3px 5px #d0d0d0;
            }
            label.label-form-color {
                color: #3c3939c7;
            }

            .div-label-form-events {
                width: 50%;
                margin: 1em auto;
                position: relative;
                span {
                    position: absolute;
                    top: -5px;
                    right: 0;
                    img {
                        width: 20px;
                        cursor: pointer;
                    }
                }

                div.question {
                    position: absolute;
                    top: 25px;
                    width: 526px;
                    color: #fff;
                    background: #34325a;
                    z-index: 99999;
                    right: 0;
                    left: -30px;
                    border: 1px solid #fff;
                    border-radius: 6px;
                    box-sizing: content-box;
                    padding: 1.5em 2.5rem;

                    > strong {
                        margin: 20px 0;
                        font-size: 22px;
                    }
                }
            }

            input {
                height: 20px;
            }

            input.input-form-event-sintoma,
            input.input-form-event-conferencia {
                &:focus {
                    color: #000000;
                    font-weight: 700;
                    background-color: #fffc;
                    border-color: #c7b2b3;
                    outline: 0;
                    box-shadow: 0 0 0 0.2rem rgba(132, 76, 6, 0.25);
                }
            }
            input.input-form-event-conferencia {
            }

            div.row {
                div {
                    width: 100%;
                    display: flex;
                    justify-content: center;
                    button {
                        margin: 0 1em;
                        width: 75px;
                    }
                }
            }
        }
    }
`;

export default StyleAddEvents;

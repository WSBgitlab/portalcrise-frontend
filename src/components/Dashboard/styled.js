import styled from "styled-components";

const DashStyle = styled.div`
    position: relative;
    width: 100vw;
    height: 100vh;
    overflow: auto;

    &::-webkit-scrollbar {
        background: transparent;
    }
    &::-webkit-scrollbar-track-piece {
        background: transparent;
        height: 3px;
    }
    &::-webkit-scrollbar-thumb {
        background: transparent;
        border: 6px solid transparent;
        border-radius: 15px;
        width: 2px;
    }
    &::-webkit-scrollbar-corner {
        background: transparent;
    }

    /* div.tables {
        &::-webkit-scrollbar {
            background: transparent;
            width: 15px;
        }
        &::-webkit-scrollbar-button {
            /* 2 
        }
        &::-webkit-scrollbar-track {
            background-color: rgba(100, 100, 100, 0.5);
        }

        &::-webkit-scrollbar-track-piece {
            background: #fff;
            height: 1px;
            width: 25% !important;
        }

        &::-webkit-scrollbar-thumb {
            background-color: #818b99;
            border: 3px solid transparent;
            border-radius: 9px;
            background-clip: content-box;
        }
        &::-webkit-scrollbar-corner {
            background: blue;
        }
    } */

    div.table-responsive {
        &::-webkit-scrollbar {
            background: transparent;
            width: 15px;
        }
        &::-webkit-scrollbar-button {
            /* 2 */
        }
        &::-webkit-scrollbar-track {
            background-color: rgba(100, 100, 100, 0.5);
        }

        &::-webkit-scrollbar-track-piece {
            background: #fff;
            height: 1px;
            width: 25% !important;
        }

        &::-webkit-scrollbar-thumb {
            background-color: #818b99;
            border: 3px solid transparent;
            border-radius: 9px;
            background-clip: content-box;
        }
        &::-webkit-scrollbar-corner {
            background: blue;
        }
    }

    div.maingrid-dash {
        header {
            span {
                h3 {
                    margin: 0;
                    font-size: 23px;
                }
            }

            hr {
                width: 50%;
                height: 4px;
                border: 1px solid #313b6b;
                border-radius: 10px;
                background-color: #313b6b;
            }
        }

        main {
            margin:3em 0 0 0;
            
            div.header-tables {
                h3 {
                    margin: 0;
                    font-size: 22px;
                    color: #4f4f4f;
                    img {
                        margin-left: 1em;
                        background-color: #fff;
                        border: 1px solid #fff;
                        border-radius: 15px;
                    }
                }
            }


            div.tables {
                width: 94%;
                margin: 2.4rem 3%;

                th.tr-header{
                    color:#fff;
                }

                tr.row-table-linked{
                    cursor:pointer;
                }
                div.pagination-grid{
                    padding: 1em;
                    justify-content: center;
                    display: flex;
                }   
            }
            /*div.boxes-events {
                display: flex;
                flex-direction: column;
                justify-content: center;
                margin-top: 2em;
                flex: unset;
                border: 1px transparent;
                border-radius: 10px;

                div.main-tables {
                    width: 98%;
                    background: #dfdfdf24;
                    margin: 2.4rem 1%;
                    padding: 15px 2em;

                    div.header-tables {
                        h3 {
                            margin: 0;
                            font-size: 22px;
                            color: #4f4f4f;
                            img {
                                margin-left: 1em;
                                background-color: #fff;
                                border: 1px solid #fff;
                                border-radius: 15px;
                            }
                        }

                        hr {
                        }
                    }
                    div.tables {
                        width: 94%;
                        margin: 2.4rem 3%;

                        div.pagination-grid{
                            padding: 1em;
                            justify-content: center;
                            display: flex;
                        }
                        
                    }

                    table {
                        width: 100%;
                        height: 500px;
                        margin: 0 auto;
                        border: 1px solid #ccc;
                        box-sizing: content-box;
                        box-shadow: 4px 4px 6px #ccc;

                        tr.header-row{
                            background: #303030;
                            color: #fff;
                            cursor: auto;
                        }
                        tr{
                            cursor: pointer;
                        }
                        th {
                            text-align: left;
                        }
                        td {
                            text-align: left;
                        }
                        td.actions {
                            width: 110px;
                            ul {
                                margin: 0;
                                padding: 0;
                                clear: both;
                                height: 100%;
                                position: relative;
                                list-style: none;
                                display: flex;
                                justify-content: center;

                                a.description {
                                    transition: 2s all;

                                    &:hover {
                                        transition: 2s all;
                                        &:before {
                                            content: "Detalhes";
                                            position: absolute;
                                            left: -15px;
                                            background: #4c54ff;
                                            padding: 1px 8px;
                                            color: #fff;
                                            border: 1px transparent;
                                            border-radius: 9px;
                                            font-size: 10px;
                                        }
                                    }
                                }

                                a.closedEvent {
                                    transition: 2s all;

                                    &:hover {
                                        transition: 2s all;
                                        &:before {
                                            content: "Detalhes";
                                            position: absolute;
                                            left: -15px;
                                            background: #4c54ff;
                                            padding: 1px 8px;
                                            color: #fff;
                                            border: 1px transparent;
                                            border-radius: 9px;
                                            font-size: 10px;
                                        }
                                    }
                                }

                                li.close {
                                    background-color: #ff1c1ccc;
                                }

                                li {
                                    float: left;
                                    margin: 0 6%;
                                    width: 25px;
                                    height: 25px;
                                    display: flex;
                                    border: 1px transparent;
                                    border-radius: 5px;
                                    background-color: #2f3034a6;
                                    justify-content: center;
                                    align-items: center;
                                    img {
                                        width: 13px;
                                        height: 13px;
                                    }
                                }
                            }
                        }
                    } 
                } 
            } */
        } /** Final Main */
    }
`;

export default DashStyle;

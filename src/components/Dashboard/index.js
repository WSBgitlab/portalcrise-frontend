import React, { useEffect, useState, useMemo } from "react";

import { useHistory } from "react-router-dom";

import Styled from "./styled";

import Menu from "../Menu/index";

import MainAppStyled from "../MainApp/styled";

import EventsOpen from "../EventsOpen/index.js";

import EventsClosed from "./../EventsClosed/index";

import EventsChange from "./../EventsChanges/index";

import EventsChangeClosed from "./../EventsChangesClosed/index";

import Api from "./../../config/api";

import { getToken } from "./../../config/auth";


export default function Dashboard() {
    //loading
    const [loading, setLoading] = useState(false);

    const history = useHistory();

    const headers = {
        token: getToken(),
    };

    document.title = "Portal Dashboard"
    
    return (
        <>
            <Menu />
            <Styled>
                <MainAppStyled className="col col-md-10 maingrid-dash">
                    <header>
                        <span>
                            <h3>Portal de Comunicação - Dashboard</h3>
                        </span>
                        <hr></hr>
                    </header>

                    <main>
                        <div className="boxes-events">
                            <div className="main-tables">
                                <EventsOpen  />
                            </div>
                        </div>

                        <div className="boxes-events">
                            <div className="main-tables">
                                <EventsClosed  />
                            </div>
                        </div>

                        <div className="boxes-events">
                            <div className="main-tables">
                                <EventsChange  /> 
                            </div>
                        </div>

                        <div className="boxes-events">
                            <div className="main-tables">
                                <EventsChangeClosed  /> 
                            </div>
                        </div>
                    </main>
                </MainAppStyled>
            </Styled>
        </>
    );
}

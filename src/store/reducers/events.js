const INITIAL_STATE = {
    activeEvent: {
        id: 0,
        sistema: "",
        ambiente: "",
        createdat: "",
        updatedat: "",
        sintoma: "",
        relevancia: "",
    },
    event: [
        {
            name: "",
            id: 0,
            causa: "",
        },
    ],
};

export default function reducers(state = INITIAL_STATE, action) {
    if (action.type === "ACTIVE_EVENT") {
        return {
            ...state,
            activeEvent: action.payload,
        };
    }

    return state;
}

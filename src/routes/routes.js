import React from "react";

import {
    BrowserRouter as Routers,
    Route,
    Switch,
    Redirect,
} from "react-router-dom";

// import { isAuthenticated } from "./../config/auth";
/**
 * Provider para publicar todas as informações do store para o restante da aplicação
 */

// Components
import Login from "./../pages/Login/index";

import Dashboard from "./../components/Dashboard/index";

import EventAdd from "./../components/EventsAdd/index";

import EventsOngoing from "../components/EventsOngoing/index";

import EventsChangeOngoing from "../components/EventsChangesOngoing/index";

import InsertNewUser from "./../components/InsertUser/index";

import ClosedEvents from "./../components/DeleteEvents/index";

import ClosedChange from "./../components/DeleteChanges/index";

import ViewRedux from "./../components/ViewRedux/index";

import ClosedEventsDetails from "./../components/EventsClosedDetails/index";

import ClosedChangesDetails from "./../components/ChangesClosedDetails/index";

import NotFound from "./../components/notFound/index";

import ResetPassword from "./../components/ResetPassword/index";

// import DataPagination from './../components/DatasPaginations/index';

import DatasPaginations from "./../components/DatasPaginations/index";

// eslint-disable-next-line react/prop-types
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) =>
            localStorage.getItem("keT") !== null ? (
                <Component {...props} />
            ) : (
                <Redirect to={{ pathname: "/", state: { from: props } }} />
            )
        }
    />
);

export default function Router() {
    return (
        <Routers>
            <Switch>
                <Route path="/" exact component={Login} />
                <PrivateRoute path="/notFound" component={NotFound} />
                <PrivateRoute path="/pagination" component={DatasPaginations} />
                <Route path="/dashboard" component={Dashboard} />
                <PrivateRoute path="/event/add" component={EventAdd} />
                <PrivateRoute path="/view/redux" component={ViewRedux} />
                <Route path="/users/new" component={InsertNewUser} />
                <PrivateRoute path="/event/ongoing/:id_event/:id_user" component={EventsOngoing} />
                <PrivateRoute path="/event/change/ongoing/:id_change/:id_user" component={EventsChangeOngoing} />
                <Route path="/reset/password/" component={ResetPassword} />
                <PrivateRoute path="/delete/events/:id_event" component={ClosedEvents} />
                <PrivateRoute path="/delete/changes/" component={ClosedChange} />
                <PrivateRoute path="/delete/events/details/:id_event" component={ClosedEventsDetails} />
                <PrivateRoute path="/delete/changes/details/:id_change" component={ClosedChangesDetails} />
            </Switch>
        </Routers>
    );
}

import Api from "./../config/api";

import { cardInformation, cardError } from "./../config/cards";

//Strings de conecção aos Grupos.

export async function newEvent(
    url,
    relevancia,
    sistema,
    ambiente,
    sintoma,
    conferencia,
    evento,
    status
) {
    try {
        let stringEvento = String(evento).toUpperCase();
        let sendMenssagem = `📣 <b>[${relevancia}]</b> - <b>${stringEvento}</b>%0A<b>Serviço</b> : ${sistema} - ${ambiente}%0A<b>Sintoma</b> : ${sintoma}%0A<b>Conferência</b> : ${conferencia}%0A<b>Status</b>:${status}%0A`;

        const response = await Api.get(`${url}${sendMenssagem}`);

        console.log(response);

        if (response.status === 200)
            return cardInformation("Msg ao telegram enviada com sucesso!");
    } catch (error) {
        cardError("Erro Interno, contate suporte!");
    }
}

export async function newEventLink(
    url,
    relevancia,
    sistema,
    ambiente,
    sintoma,
    conferencia,
    evento,
    status
) {
    try {
        let stringEvento = String(evento).toUpperCase();
        let sendMenssagem = `📣 <b>[LINK]</b> - <b>${stringEvento}</b>%0A<b>Link</b> : ${sistema} - ${ambiente}%0A<b>Sintoma</b> : ${sintoma}%0A<b>Conferência</b> : ${conferencia}%0A<b>Status</b>:${status}%0A`;

        const response = await Api.get(`${url}${sendMenssagem}`);

        console.log(response);

        if (response.status === 200)
            return cardInformation("Msg ao telegram enviada com sucesso!");
    } catch (error) {
        cardError("Erro Interno, contate suporte!");
    }
}

export async function newEventChange(url, change, description, start) {
    try {
        let sendMenssagem = `📣 <b>[${change}]</b>%0A<b>Muçanda</b> : ${description} %0A<b>Início</b> : ${start}%0A%0A`;
        const response = await Api.get(`${url}${sendMenssagem}`);
        // console.log(response);
        if (response.status === 200)
            return cardInformation("Msg ao telegram enviada com sucesso!");
    } catch (error) {
        cardError("Erro Interno, contate suporte!");
    }
}

export async function closedEvent(
    url,
    relevancia,
    evento,
    sistema,
    ambiente,
    sintoma,
    status
) {
    try {
        let stringEvento = String(evento).toUpperCase();

        console.log(evento);

        console.log(status);

        let sendMenssagem = `✅ <b>[${relevancia}]</b> - <b>${stringEvento}</b>%0A<b>Serviço</b> : ${sistema} - ${ambiente}%0A<b>Sintoma</b> : ${sintoma}%0A<b>Status</b> : ${status}`;
        const response = await Api.get(`${url}${sendMenssagem}`);

        if (response.status === 200)
            return cardInformation("Enviado ao Telegram!");
    } catch (erro) {
        cardError("Erro Interno, contate suporte!");
    }
}

export async function statusForTelegram(
    url,
    mensseger,
    relevancia,
    evento,
    sistema,
    ambiente,
    sintoma
) {
    try {
        let sendMenssagem = `📣 <b>[INFORMATIVO]</b> - ${evento}%0A<b>Serviço</b> : ${sistema}%0A<b>Ambiente</b> : ${ambiente}%0A<b>Sintoma</b> : ${sintoma}%0A<b>Status</b> : ${mensseger}%0A%0A`;
        const response = await Api.get(`${url}${sendMenssagem}`);

        if (response.status === 200)
            return cardInformation("Enviado ao Telegram!");
    } catch (err) {
        if (err) throw err;
        cardError("Erro Interno, contate suporte!");
    }
}

import axios from "axios";

import https from "https";

const headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "X-XSS-Protection": "1"
};

const api = axios.create({
    baseURL: "http://localhost:8080",
    httpsAgent: new https.Agent({
        rejectUnauthorized: false,
    }),
    headers,
});

export default api;
